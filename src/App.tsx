import React, { Component } from "react";
import "./styles.css";
import data from "./data/response.json";
import StyledSelect from './view/StyledSelect'
import {LabelType, NewLabelType} from './interface/interface'

const groupByCategory = (labels: LabelType[]) => {
  // Create a unique set of categories
  let set_categories: string[] = [...Array.from(new Set(labels.map((item) => item.category)))]

  // Then we iterate through label to group by category
  let new_labels: NewLabelType[] = set_categories.map(category => {
    return {
      category: category,
      names: [],
    };
  })
  labels.forEach(label => {
    new_labels.forEach(item => {
      if (label.category === item.category) {
        item.names.push(label.name)
      }
    })
  })
    return new_labels;
};

class App extends Component<{}, { select_items: NewLabelType[] }> {
  constructor(props: any) {
    super(props);

    this.state = {
      select_items: groupByCategory(data.labels),
    };
  }

  render() {
    const { select_items } = this.state;
    return <StyledSelect select_items={select_items} />;
  }
}

export default App;
