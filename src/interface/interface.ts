
// typeof each label in response.json
export type LabelType = {
  label_id: string,
  org_id: string,
  category: string,
  name: string,
}

export type NewLabelType = {
    category: string,
    names: string[]
}