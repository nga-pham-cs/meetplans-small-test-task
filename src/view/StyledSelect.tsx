import React, { FunctionComponent } from "react";
import "../styles.css";
import { Select, FormControl } from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import {NewLabelType} from '../interface/interface'

type StyledSelectProps = {
  select_items: NewLabelType[];
};

const StyledSelect: FunctionComponent<StyledSelectProps> = (props) => {
  const { select_items } = props;
  const [state, setState] = React.useState({
    option: "",
  });

  const handleChange = (
    event: React.ChangeEvent<{ name?: any; value: any }>
  ) => {
    setState({
      ...state,
      option: event.target.value,
    });
  };

  return (
    <FormControl>
      <InputLabel htmlFor="grouped-native-select">Label</InputLabel>
      <Select
        native
        id="grouped-native-select"
        value={state.option}
        onChange={handleChange}
        placeholder="Placeholder"
        className="Select"
      >
        {select_items.map((item) => (
          <optgroup label={item.category}>
            {item.names.map(name => (
              <option value={name}>{name}</option>
            ))}
          </optgroup>
        ))}
      </Select>
    </FormControl>
  );
};

export default StyledSelect;
