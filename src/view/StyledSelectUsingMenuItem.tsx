import React, { FunctionComponent } from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/styles";
import { Select, FormControl, ListSubheader } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import InputLabel from "@material-ui/core/InputLabel";

const GroupItems = (props: {select_items: string[]}) => {
  const {select_items} = props
  return (
    <>
      <ListSubheader className="Optgroup">Category</ListSubheader>
      {select_items.map((item) => (
        <MenuItem value={item}>{item}</MenuItem>
      ))}
    </>
  );
};

export const StyledSelect: FunctionComponent<{ select_items: string[] }> = (props) => {
  const { select_items } = props;
  const [state, setState] = React.useState({
    option: "",
  });

  const handleChange = (
    event: React.ChangeEvent<{ name?: any; value: any }>
  ) => {
    console.log(event.target);
    setState({
      ...state,
      option: event.target.value,
    });
  };

  return (
    <FormControl>
      <InputLabel htmlFor="grouped-native-select">Label</InputLabel>
      <Select
        native
        defaultValue=""
        id="grouped-native-select"
        value={state.option}
        onChange={handleChange}
        // input={<StyledInput />}
        className="Select"
        autoWidth={true}
      >
        <GroupItems select_items={select_items} />
      </Select>
    </FormControl>
  );
};

export default StyledSelect